** Abstract **
Every year my family travels to Michigan over Thanksgiving to celebrate both Thanksgiving and Christmas at the same time.  There are about 20 of us that gather.  Each year there are countless google docs, group texts, excel sheets, etc. to organize what everyone will be bringing. I wanted to make a family website for my project to make things easier for everyone. Now there will be one place that everyone can go to in order to find the answers to all of their questions.

This site is very self explanatory and easy to use.  I think the only thing needing special instructions are the links and the javascript function. You may not be able to open the links because of access abilities but my whole family has access to the photos link and the food link.
The javascript function is at the bottom of the food page.  I thought we could use it to add any other input or any other items they might want to bring.  I have tested it multiple times and when you press the sign up button the text that was typed in shows up at the top of the list.

I have tried to open it in multiple different browsers and have found that some of my headings and hover tags don't always work the exact same, but other than that I got everything on the list you sent!

Hope you enjoy!!

** File Structure **
lay out the file structure "how do I get this on my machine?"
File Structure
The project folder is Seets-Christmas and contains pages of HTML that create a website.
The HTML pages all have .css files used for styling in the css folder
img contains all resources required to view the website
app.js contains a javascript function so that people can sign up to bring items to the potluck dinner.
Entrypoint
The entrpoint into the website is index.html and to view it clone the git repo to your local machine and open file://path-to-folder/SEETS-CHRISTMAS/index.html in your browser



** Assignment overview **
Readme has been updated
viewport tag was added to all html pages
the syntax error on index.html was resolved
media query was used to change background color of the body depending on view size