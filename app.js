
function addItemToTable(){
    var table = document.getElementById("potluckList");
    // Create an empty <tr> element and add it to the 1st position of the table:
    var row = table.insertRow(table.rows.length-1);
    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    // Add some text to the new cells:
    cell1.innerHTML = document.getElementById("name").value;
    cell2.innerHTML = document.getElementById("item").value;
    cell3.innerHTML = document.getElementById("amount").value;
}